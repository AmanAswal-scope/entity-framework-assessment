﻿using BusinessObjects.Models;

namespace BusinessObjects.Models
{
    public class User
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime LastUpdatedAt { get; set; }
        public bool IsActive { get; set; }
        public Role Role { get; set; }


    }
}
