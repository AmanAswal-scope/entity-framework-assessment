﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects.Models
{
    public class Order
    {
        public int OrderID { get; set; }
        public DateTime CreatedAt { get; set; }
        public User User { get; set; }

        public Product Product { get; set; }
    }
}

