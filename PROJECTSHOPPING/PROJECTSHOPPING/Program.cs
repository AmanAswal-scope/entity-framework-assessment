﻿

using BusinessObjects.Models;
using ShoppingSite.BAL;
using System.Data;

namespace PROJECTSHOPPING
{
    internal class Program
    {

        static Bal bal = new Bal();
        static void AddUser()
        {
            Console.WriteLine("Enter Name");
            string userName = Console.ReadLine();
            Console.WriteLine("Enter Role:\n2.Supplier\n3.Customer");
            int roleid = int.Parse(Console.ReadLine());
            Role role = bal.GetRole(roleid);
            User user = new User()
            {
                UserName = userName,
                Role = role,
                IsActive = true
            };

            int response = bal.AddUser(user);
            if (response == 1)
            {
                Console.WriteLine("User has been added");
            }
        }






        static void DeleteUser()
        {
            Console.WriteLine("Enter User Id:");
            int userid = int.Parse(Console.ReadLine());
            int response = bal.DeleteUser(userid);
            if (response == 1)
            {
                Console.WriteLine("User Deleted !!!!");
            }
            else
            {
                Console.WriteLine("Error");
            }
        }






        static void GetUser()
        {
            Console.WriteLine("Enter User Id:");
            int userid = int.Parse(Console.ReadLine());
            User user = bal.GetUser(userid);
            if (user != null)
            {
                Console.WriteLine("User name: " + user.UserName);
                Console.WriteLine("User role: " + user.Role.RoleName);
            }
            else
            {
                Console.WriteLine("User does not exist.");
            }
        }






        public static void AddProduct()
        {
            Console.WriteLine("Enter Product Name:");
            string productName = Console.ReadLine();
            Console.WriteLine("Enter Product Price:");
            float productPrice = float.Parse(Console.ReadLine());
            Console.WriteLine("Enter Product Quantity:");
            int productQty = int.Parse(Console.ReadLine());
            Product product = new Product()
            {
                ProductName = productName,
                Price = productPrice,
                Quantity = productQty
            };

            int response = bal.AddProduct(product);
            if (response == 1)
            {
                Console.WriteLine("Product has been added");
            }
        }





        public static void DeleteProduct()
        {
            Console.WriteLine("Enter Product Id:");
            int productid = int.Parse(Console.ReadLine());
            int response = bal.DeleteProduct(productid);
            if (response == 1)
            {
                Console.WriteLine("Product Deleted Successfully");
            }
            else
            {
                Console.WriteLine("Some Problem Occured");
            }
        }






        public static void UpdateProduct()
        {
           /* Console.WriteLine("Enter Product Id:");
            int proId = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter Product Price:");
            float editedproductPrice = float.Parse(Console.ReadLine());
            Console.WriteLine("Enter Product Quantity:");
            int editedproductQty = int.Parse(Console.ReadLine());


            int response = bal.UpdateProduct(proId, editedproductPrice, editedproductQty);



            if (response == 1)
            {
                Console.WriteLine("Product has been edited");
            }
            else
                Console.WriteLine("not added try again!");*/






            Console.WriteLine("Enter the name of the product to edit");
            string name = Console.ReadLine();

           
                Console.WriteLine("Enter the new name");
                string? ProductName = Console.ReadLine();
                Console.WriteLine("Enter the new price");
                //int Price = Convert.ToInt32(Console.ReadLine());
                int Price;
                Int32.TryParse(Console.ReadLine(), out Price);
                Console.WriteLine("Enter the new Quantity");
                int Quantity;
                Int32.TryParse(Console.ReadLine(), out Quantity);
                Product upDatedProduct = new Product()
                {
                    ProductName = ProductName,
                    Price = Price,
                    Quantity = Quantity
                };
                bal.UpdateProduct(name, upDatedProduct);
            
        }


        /*  public int PlaceOrder(Order order)
          {
              List<Product> products = db.Products.ToList();
              foreach (Product product in products)
              {
                  Console.WriteLine(product.ProductID + " " + product.ProductName + " " + product.Price + " " +
                  product.Quantity);
              }
              Console.WriteLine("select product id");
              int prodId = Convert.ToInt32(Console.ReadLine());
              List<User> users = db.users.ToList();
              foreach (User usr in users)
              {
                  Console.WriteLine(usr.UserID + " " + usr.UserName + " ");
              }
              Console.WriteLine("select user id");
              int userId = Convert.ToInt32(Console.ReadLine());



              Product ProductObj = db.products.Find(prodId);
              User UserObj = db.users.Find(userId);



              order.Product = ProductObj;
              order.User = UserObj;



              db.Orders.Add(order);
              db.SaveChanges();
              Console.WriteLine("Order has been placed!");
              return 0;



          }
  */



        public static void GetProducts()
        {
            var products = bal.GetProducts();
            if (products.Count > 0)
            {
                foreach (Product prod in products)
                {
                    Console.WriteLine(prod.ProductID + " " + prod.ProductName + " " + prod.Price + " " + prod.Quantity);
                }
            }
            else { Console.WriteLine("       No Products       "); }
        }

        public static void PlaceOrder()
        {
            Console.WriteLine("Enter UserId:");
            int userId = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter ProductId:");
            int productId = int.Parse(Console.ReadLine());
            User user = bal.GetUser(userId);
            Product product = bal.GetProduct(productId);
            Order ord = new Order()
            {
                User = user,
                Product = product,
                CreatedAt = DateTime.Now,
            };

            int response = bal.PlaceOrder(ord);
            if (response == 1)
            {
                Console.WriteLine("Your order has been placed.");
            }
            else
            {
                Console.WriteLine("error");
            }
        }























        static void Main(string[] args)
        {
            int choice;
            string role;
            Console.WriteLine("Enter your Role:");
            role = Console.ReadLine();
            if (role.ToLower() == "admin")
            {
                char ch = 'y';
                while (ch == 'y')
                {
                    Console.WriteLine("  Menu  ");
                    Console.WriteLine("1.Add User\n2.Delete User\n3.Get User");
                    choice = int.Parse(Console.ReadLine());
                    switch (choice)
                    {
                        case 1: AddUser(); break;
                        case 2: DeleteUser(); break;
                        case 3: GetUser(); break;
                        default: Console.WriteLine("Wrong Input"); break;
                    }
                    Console.WriteLine("Want to continue y/n:");
                    ch = Convert.ToChar(Console.ReadLine());
                }
            }
            else if (role.ToLower() == "supplier")
            {
                char ch = 'y';
                while (ch == 'y')
                {
                    Console.WriteLine("  Menu  ");
                    Console.WriteLine("1.Add Product\n2.Delete Product\n3.Edit Product");
                    choice = int.Parse(Console.ReadLine());
                    switch (choice)
                    {
                        case 1: AddProduct(); break;
                        case 2: DeleteProduct(); break;
                        case 3: UpdateProduct(); break;
                        default: Console.WriteLine("Wrong Input"); break;
                    }
                    Console.WriteLine("Want to continue y/n:");
                    ch = Convert.ToChar(Console.ReadLine());
                }

            }

            else if (role.ToLower() == "customer")
            {
                char ch = 'y';
                while (ch == 'y')
                {
                    Console.WriteLine("  Menu  ");
                    Console.WriteLine("1.Get All Products\n2.To Add Order\n");
                    choice = int.Parse(Console.ReadLine());
                    switch (choice)
                    {
                     //   case 1: GetAddProduct(); break;
                      //  case 2: AddOrder(); break;
                        default: Console.WriteLine("Wrong Input"); break;
                    }
                    Console.WriteLine("Want to continue y/n:");
                    ch = Convert.ToChar(Console.ReadLine());
                }



            }

        }
    }
}
