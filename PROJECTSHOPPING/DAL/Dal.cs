﻿using BusinessObjects.Models;
using DAL;
using Microsoft.Identity.Client;

namespace DAL
{
    public class Dal
    {
        ShopDbContext db = new ShopDbContext();

        public int AddUser(User user)
        {
            db.Users.Add(user);
            db.SaveChanges();
            return 1;
        }

        public int DeleteUser(int userid)
        {
            User user = db.Users.Find(userid);
            if (user != null)
            {
                user.IsActive = false;
                db.SaveChanges();
                return 1;
            }
            return 0;
        }

        public User GetUser(int id)
        {
            User user = db.Users.Find(id);
            if (user != null)
            {
                return user;
            }
            return null;
        }

        public Role GetRole(int id)
        {
            Role role = db.Roles.Find(id);
            return role;
        }

        public int AddProduct(Product prod)
        {
            db.Products.Add(prod);
            db.SaveChanges();
            return 1;
        }

        public int DeleteProduct(int productid)
        {
            Product prod = db.Products.Find(productid);
            if (prod != null)
            {
                db.Products.Remove(prod);
                db.SaveChanges();
                return 1;
            }
            return 0;
        }


        /*
                 public int UpdateProduct(int proId,float price,int qnt)
                    {
                        Product prod = db.Products.Find(proId);
                        if (prod != null)
                        {
                            prod.Price = price;
                            prod.Quantity = qnt;
                            db.SaveChanges();
                            return 1;
                        }
                        return 0;
                    }
        */

        public int UpdateProduct(string name, Product product)
        {
            Product obj = db.Products.Where(x => x.ProductName == name).FirstOrDefault();
            if (obj != null)
            {
                if (product.ProductName.Length > 0)
                    obj.ProductName = product.ProductName;
                if (product.Price > 0)
                    obj.Price = product.Price;

                if (product.Quantity > 0)
                    obj.Quantity = product.Quantity;

                db.Products.Update(obj);
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }


        public Product GetProduct(int id)
        {
            Product prod = db.Products.Find(id);
            if (prod != null)
            {
                return prod;
            }
            return null;
        }

        public List<Product> GetProducts()
        {
            List<Product> products = new List<Product>();
            products = db.Products.ToList();
            return products;
        }

        public int PlaceOrder(Order ord)
        {
            db.Orders.Add(ord);
            db.SaveChanges();
            return 1;
        }




    }

}